'use strict';

var configFile = 'config.json';
var fs = require('fs');

var exports = module.exports = {};

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (obj, fromIndex) {
		if (fromIndex == null) {
			fromIndex = 0;
		} else if (fromIndex < 0) {
			fromIndex = Math.max(0, this.length + fromIndex);
		}
		for (var i = fromIndex, j = this.length; i < j; i++) {
		if (this[i] === obj)
			return i;
		}
		return -1;
	};
}

exports.init = async function(onConfigRead, configToRead) {
	try {
		await exports.readConfig(configToRead, onConfigRead);
	} catch (e) {
		console.log("Exception while reading config " + configToRead);
		console.log(e);
	}
};

exports.readConfig = async function (fileToRead, onConfigRead) {
	if (!fileToRead) {
		fileToRead = configFile;
	}
	fs.readFile(fileToRead, 'utf8', function (err, data) {
		console.log("Reading config from " + fileToRead);
		var config = JSON.parse(data);
		exports.config = config;
		onConfigRead(config);
		return config;
	});
};

exports.isListenChannel = function(channelList, channel) {
	console.debug("Checking channel list against channel " + channel);
	if (channelList) {
		console.debug("Channel has " + channelList.length + " values.");
		for (var i = 0; i < channelList.length; i++) { 
			var check = channelList[i];
		
			if (check == channel) {
				console.debug("Matched channel " + i + " of " + channel + " against " + check);
				return true;
			}
		}
	}
	console.log("Returning - async check");
	return false;
}