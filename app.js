'use strict';

const Discord = require('discord.js');
//const SlackBot = require('slackbots');
const pinnypals = require('./pinnypals');

const configLoader = require('./config');
var CONFIG;
var ALL_PINS;
const MAX_RESULTS = 3;

const discord = new Discord.Client();
var slack;

const configFile = 'config.json';

discord.on('ready', () => {
  console.log('I am ready!');
});

function findSetById(sets, setId) {
	console.debug("Searching for " + setId + " in " + sets.length + " sets.");
	for (var i = 0;i < sets.length;i++) {
		var set = sets[i];
		if (set.id == setId) {
			console.debug("Matched pin set " + setId);
			return set;
		} 
	}
	return null;
}

function buildPinSetString(pin, sets) {
	var set = findSetById(sets, pin.set_id);
	var str = null;
	if (set != null) {
		if (set.tradeable == "Y") {
			str = set.name + " set - ";
		} else {
			str = pin.year + " " + set.name + " - ";
		}
	}
	return str;
}

function buildResultMessage(pins, searchterm, sets) {
	var firstline;
	if (pins.length == 1) {
		firstline = "I found this result for \"" + searchterm + "\": \n";
	} else {
		firstline = "I found " + pins.length + " results for \"" + searchterm + "\", but this was the best match: \n"; 
	}
	var pin = pins[0];
	var pinImageUrl = "https://pinnypals.com/imgs/" + pin.image_name;
	var pinnypalsUrl = "https://pinnypals.com/pin/" + pin.id;
	var msgText;
	
	var setString = buildPinSetString(pin, sets);
	if (setString != null) {
		msgText = firstline + setString + pin.name + " - " + pinnypalsUrl; 
	} else {
		msgText = firstline + pin.name + " - " + pinnypalsUrl;
	}
	 
	var result = {
		files: [ pinImageUrl ],
		url: pinnypalsUrl,
		message: msgText
	};
	return result;
}

// Create an event listener for messages
discord.on('message', message => {
	if (message.channel && message.channel.name) {
		if (configLoader.isListenChannel(CONFIG.channels, message.channel.name)) {
			// If the message is "ping"
			if (message.content === 'ping') {
				// Send "pong" to the same channel
				message.channel.send('pong');
			}
			if (message.content.startsWith("!pin ")) {
				var searchterm = message.content.substring(5);
				var pins = pinnypals.findPins(searchterm, ALL_PINS.pins);
				
				if (pins.length > 0) {
					var result = buildResultMessage(pins, searchterm, ALL_PINS.sets);
					
					var msgObj = {
						message: result.message, 
						files: result.files
					};
					
					var msgOpts = new Discord.MessageEmbed({
						files: result.files,
						url: result.url
					});
					message.reply(result.message, msgOpts);
				} else {
					message.react(CONFIG.discord.reactions.notfound)
						.then(console.log)
 						.catch(console.error);
				}
			}
		}
	}
});

function pinLoadFailure(err) {
	console.error("Failed loading pins: " + err);
}
function buildResultString(pins, searchterm) {
	var result = buildResultMessage(pins, searchterm, ALL_PINS.sets);
	return result;
}

function buildBestMatch(pin) {

}

function pinsLoaded(pinData) {
	ALL_PINS = pinData;
	if (process.argv.length > 2) {
		var args = process.argv.slice(2);
		var searchterm = args[0];
		console.log("Performing search for " + searchterm);
		var pins = pinnypals.findPins(searchterm, ALL_PINS.pins);
		console.log("Found " + pins.length + " matching pins.");
		
		var results = buildResultString(pins, searchterm);
		console.log(JSON.stringify(results));
		for (var i = 0;i < MAX_RESULTS && i < pins.length;i++) {
			console.log("pin[" + i + "]: " + JSON.stringify(pins[i]));
		}
	} else {
		console.debug = function() {}
		startBots();
	}
}

function startBots() {
//	slack = new SlackBot({token: CONFIG.slack.token,
//    name: 'Pinboy'
//	});
//	initSlackbot(slack);
//	console.log('Created slack bot.');

	// Log our bot in using the token from https://discord.com/developers/applications
	discord.login(CONFIG.discord.token);
}

configLoader.init(function(configData) {
	CONFIG = configData;
	pinnypals.loadPins(CONFIG.pinnypals.allpins, CONFIG.pinnypals.cachefile, pinsLoaded, pinLoadFailure);

}, configFile);


function initSlackbot(bot) {
	bot.on('start', () => {
		console.log('Slackbot started.');
	});
	
	bot.on('error', (err) => {
		console.log(err);
	});
}
