FROM node:12.18.3-alpine

WORKDIR /pinboy

COPY package*.json /pinboy/
RUN npm install

COPY app.js /pinboy/app.js
COPY package.json /pinboy/package.json
COPY config.js /pinboy/config.js
COPY pinnypals.js /pinboy/pinnypals.js

COPY config.json /pinboy/config.json

CMD ["node", "app.js"]