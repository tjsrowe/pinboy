'use strict';

const request = require('request');
const fs = require('fs');
const path = require('path');

function ensureDirectoryExistence(filePath) {
	var dirname = path.dirname(filePath);
	if (fs.existsSync(dirname)) {
		return true;
	}
	ensureDirectoryExistence(dirname);
	fs.mkdirSync(dirname);
}

var exports = module.exports = {};

exports.loadPins = function(pinQueryUrl, localCache, onSuccess, onFail) {
	var options = {json: true};
	
	if (fs.existsSync(localCache)) {
		fs.readFile(localCache, 'utf8', function (err, data) {
			console.log("Cached pins read from " + localCache);
			var pinData = JSON.parse(data);
			onSuccess(pinData);
		});
	} else {
		request(pinQueryUrl, options, (error, res, body) => {
		    if (error) {
		        return  console.log(error)
		        onFail(error);
		    };
		
		    if (!error && res.statusCode == 200) {
		        // do something with JSON, using the 'body' variable
		        //var pinData = JSON.parse(body);
		        var pinData = body;
		        
		        ensureDirectoryExistence(localCache);
		        fs.writeFileSync(localCache, JSON.stringify(pinData), 'utf8');
		        
		        console.log("Pins retrieved from " + pinQueryUrl + " and written to " + localCache);
		        onSuccess(pinData);
		    };
		});
	}
}

const PAXS = [
	{
	    "id": "1",
	    "name": "prime"
	}, {
	    "id": "2",
	    "name": "east"
	}, {
	    "id": "3",
	    "name": "aus"
	}, {
	    "id": "4",
	    "name": "pax"
	}, {
	    "id": "5",
	    "name": "online"
	}, {
	    "id": "6",
	    "name": "limited"
	}, {
	    "id": "7",
	    "name": "gaming"
	}, {
	    "id": "8",
	    "name": "south"
	}, {
	    "id": "9",
	    "name": "unplugged"
	}
];

function wordToPaxId(word) {
	for (var p = 0;p < PAXS.length;p++) {
		if (PAXS[p].name == word.toLowerCase()) {
			return PAXS[p];
		}
	}
	return null;
}

exports.findPins = function(searchterm, all) {
	var results = Array();
	
	var example = {
            "id": "1",
            "name": "Flesh Reaper",
            "set_id": "1",
            "sub_set_id": null,
            "year": "2013",
            "pax_id": "4",
            "alternate": "N",
            "image_name": "pin_fleshreaper.png?1"
        };
	for (var i = 0;i < all.length;i++) {
		var currentPin = Object.assign({}, all[i]);
		currentPin.score = 0;

		var searchWords = searchterm.split(" ");
		var pinWords = currentPin.name.split(" ").length;
		for (var w = 0;w < searchWords.length;w++) {
			var word = searchWords[w].toLowerCase();
			if (word == currentPin.year) {
				currentPin.score = currentPin.score + 100;
				console.debug("Year: " + word);
				continue;
			}
			var paxId = wordToPaxId(word);
			if (paxId != null && paxId.id == currentPin.pax_id) {
				currentPin.score = currentPin.score + 50;
				console.debug("PAX: " + word);
				continue;
			} else {
				console.debug("Word " + word + " matches no pax.");
			}
			if (currentPin.name.toLowerCase().includes(word)) {
				var wordScore = 100 / pinWords;
				currentPin.score += wordScore;
				console.debug("Word: " + word);
			} else {
				console.debug("Unmatched because name \"" + currentPin.name.toLowerCase() + "\" does not include \"" + word + "\"");
				currentPin.score = 0;
				break;
			}
		}
		
		if (currentPin.score > 0) {
			results.push(currentPin);
		}
	}
	results.sort(compareScore);
	results = results.reverse();
	return results;
}

function compareScore(a, b) {
	return a.score - b.score;
}
